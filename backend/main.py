from os import environ
from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from google.oauth2 import id_token
from google.auth.transport import requests
from pydantic import BaseModel

request = requests.Request()

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins = [ 'http://localhost:5173' ],
    allow_credentials = True,
    allow_methods=["*"],
    allow_headers=["*"],
)

CLIENT_ID = environ.get( 'CLIENT_ID' )
DATABASE = environ.get( 'EMAIL_ADDRESSES' ).split( ',' )


class GoogleLogin( BaseModel ):
    credential: str

@app.post("/login/google")
async def login_google( body: GoogleLogin ):
    id_info = id_token.verify_oauth2_token( body.credential, request, CLIENT_ID )

    if id_info['email'] not in DATABASE:
        raise HTTPException(
            status_code=403, detail="not authorized"
        )

    return id_info

